Imports System.IO
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports XmlToTextArchive.XmlArchiver

Module Main

    Dim runtimeErrors As StreamWriter

    Sub Main()

        Try
            Dim archiver As XmlArchiver = New XmlArchiver()

            'Archive old (delayed) articles first
            archiver.ProcessDelayedArticles()

            'Find new XML articles to archive
            archiver.ScanForXmlArticles()

            'Process Archiving
            archiver.ProcessArticles()
        Catch e As Exception
            'Runtime error, log these
            runtimeErrors = New StreamWriter(AppSettings("logFolder") & "runtime-errors.txt", True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
            runtimeErrors.AutoFlush = True
            runtimeErrors.WriteLine(Now().ToShortDateString() & " - " & Now().ToLongTimeString() & " :")
            runtimeErrors.WriteLine(e.Message)
            runtimeErrors.WriteLine(e.StackTrace & vbCrLf)
        End Try

        'Wait
        'Console.Read()
    End Sub

End Module
