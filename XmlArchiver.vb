Imports System.IO
Imports System.Xml
Imports System.Web.mail
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports ArchiveConvertLibrary

Public Class XmlArchiver

    'Variables for the recursive directory lister
    Private fileList As ArrayList = New ArrayList()

    'Arraylist to store the article headers for the articles to be archived    
    Private xmlHdrList As ArrayList = New ArrayList()

    'List of skipped files, used for moving or futher processing
    Private skipList As ArrayList = New ArrayList()

    'List of delayed files, used for moving delayed articles
    Private delayList As ArrayList = New ArrayList()

    'List of erroenous XML files, used for debugging
    Private errorList As ArrayList = New ArrayList()

    'Folders
    Private xmlFolder As String
    Private archiveFolder As String
    Private skipFolder As String
    Private delayFolder As String
    Private errorFolder As String
    Private logFolder As String

    'Counters
    Private dateCount As Hashtable = New Hashtable()
    Private archiveCount As Integer
    Private delayCount As Integer
    Private errorCount As Integer
    Private skipCount As Integer

    'Output files/folder
    Private targetFile As StreamWriter
    Private targetLogFile As StreamWriter

    Private targetFolder As String
    Private dataFileName As String
    Private logFileName As String

    Private enableTargetCopy As Boolean
    Private copyTarget As String

    Private enableFTP As Boolean
    Private FTPCmd As String
    Private FTPArgs As String
    Private FTPLog As String

    Private errorMail As String

    'Logfiles
    Private archiveLog As StreamWriter
    Private skipLog As StreamWriter
    Private delayLog As StreamWriter
    Private errorLog As StreamWriter

    'Options
    Private fileFilter As String
    Private recurseSubdirs As Boolean
    Private moveOnArchive As Boolean
    Private moveOnSkip As Boolean
    Private featureDelay As Integer
    Private generalDelay As Integer

    'Debug article id's / titles
    Private enableDebug As Boolean
    Private debugLogFolder As String


    Public Sub New()

        'Initialize vars and load settings
        xmlFolder = AppSettings("xmlFolder")
        archiveFolder = AppSettings("archiveFolder")
        skipFolder = AppSettings("skippedFolder")
        delayFolder = AppSettings("delayFolder")
        errorFolder = AppSettings("errorFolder")
        logFolder = AppSettings("logFolder")
        targetFolder = AppSettings("targetFolder")
        copyTarget = AppSettings("copyTargetFolder")
        debugLogFolder = AppSettings("debugLogFolder")

        dataFileName = AppSettings("dataFileName")
        logFileName = AppSettings("logFileName")

        FTPCmd = AppSettings("FTPCommand")
        FTPArgs = AppSettings("FTPArguments")
        FTPLog = AppSettings("FTPLog")
        errorMail = AppSettings("errorEmail")


        If AppSettings("moveArchived") = "true" Then moveOnArchive = True Else moveOnArchive = False
        If AppSettings("moveSkipped") = "true" Then moveOnSkip = True Else moveOnSkip = False
        If AppSettings("scanSubdirs") = "true" Then recurseSubdirs = True Else recurseSubdirs = False
        If AppSettings("enableTargetCopy") = "true" Then enableTargetCopy = True Else enableTargetCopy = False
        If AppSettings("enableFTPTransfer") = "true" Then enableFTP = True Else enableFTP = False
        If AppSettings("enableDebugLogging") = "true" Then enableDebug = True Else enableDebug = False

        fileFilter = AppSettings("fileFilter")
        generalDelay = AppSettings("generalDelay")
        featureDelay = AppSettings("featureDelay")

        If Not Directory.Exists(archiveFolder) Then Directory.CreateDirectory(archiveFolder)
        If Not Directory.Exists(skipFolder) Then Directory.CreateDirectory(skipFolder)
        If Not Directory.Exists(delayFolder) Then Directory.CreateDirectory(delayFolder)
        If Not Directory.Exists(errorFolder) Then Directory.CreateDirectory(errorFolder)
        If Not Directory.Exists(logFolder) Then Directory.CreateDirectory(logFolder)
        If Not Directory.Exists(targetFolder) Then Directory.CreateDirectory(targetFolder)
        If Not Directory.Exists(copyTarget) And enableTargetCopy Then Directory.CreateDirectory(copyTarget)
        If Not Directory.Exists(debugLogFolder) And enableDebug Then Directory.CreateDirectory(debugLogFolder)

        'Open targetfiles
        Dim tf As String = targetFolder & AppSettings("dataFileName").Replace("d%", Now().ToString("yyyy-MM-dd")) & ".txt"
        targetFile = New StreamWriter(tf, False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        targetFile.AutoFlush = True

        Dim lf As String = targetFolder & AppSettings("logFileName").Replace("d%", Now().ToString("yyyy-MM-dd")) & ".txt"
        targetLogFile = New StreamWriter(lf, False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        targetLogFile.AutoFlush = True

        'Open logs
        archiveLog = New StreamWriter(logFolder & Now().ToString("yyyy-MM-dd") & "-archived.txt", False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        archiveLog.AutoFlush = True

        skipLog = New StreamWriter(logFolder & Now().ToString("yyyy-MM-dd") & "-skipped.txt", False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        skipLog.AutoFlush = True

        delayLog = New StreamWriter(logFolder & Now().ToString("yyyy-MM-dd") & "-delayed.txt", False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        delayLog.AutoFlush = True

        errorLog = New StreamWriter(logFolder & Now().ToString("yyyy-MM-dd") & "-errors.txt", False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        errorLog.AutoFlush = True

    End Sub

    'Scans given XML folder for articles to archive
    Public Function ScanForXmlArticles()

        'Vars
        'Hashtable for the headers of the XML articles to be archived
        Dim xmlHeaderHash As Hashtable = New Hashtable()

        'Hashtable to store list of articles to be delayed
        Dim delayHash As Hashtable = New Hashtable()

        'Debug article info helper hashes, used only if debugging is enabled
        Dim IdToSO As Hashtable
        Dim SOToId As Hashtable
        Dim debugLogFile As String
        Dim debugLog As StreamWriter

        'Debugging enabled?
        If enableDebug Then
            IdToSO = New Hashtable()
            SOToId = New Hashtable()

            debugLogFile = Now().ToString("yyyy-MM-dd") & "-ID-errors.txt"
            debugLog = New StreamWriter(debugLogFolder & debugLogFile, False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
            debugLog.AutoFlush = True
        End If

        Dim fil As String
        Dim ver As String
        Dim xml As XmlDocument = New XmlDocument()

        fileList.Clear()
        skipList.Clear()

        'Check specified directory
        If Directory.Exists(xmlFolder) Then

            'Scanning directory
            ScanDir(xmlFolder, fileFilter, recurseSubdirs)

            'Looping through
            For Each fil In fileList

                'Load headers and stuff
                Dim hdrs As ConvertLibrary.xmlArticleHeader

                Try
                    'Load the headers
                    ConvertLibrary.LoadArticleHeaders(fil, hdrs)

                Catch
                    'File does not match NITF format, it will be skipped
                    hdrs.fileFolder = fil.Substring(0, fil.LastIndexOf("\") + 1)
                    hdrs.fileName = fil.Substring(fil.LastIndexOf("\") + 1)
                End Try

                If hdrs.stoffGruppe = "" Then
                    errorList.Add(hdrs)
                    GoTo Next_Item
                End If

                'Content check: Different actions are taken based on the content of the article

                If hdrs.target = "Ut-Feature" Then
                    'Feature articles are delayed

                    If hdrs.underGruppe = "Teaser" Then
                        'Skip teasers
                        skipList.Add(hdrs)
                    ElseIf delayHash.ContainsKey(hdrs.id) Then
                        'Check versions/timestamp for features too? Yepp, we have too...
                        'Article already exists
                        'Update if necessary and skip old version
                        Dim old As ConvertLibrary.xmlArticleHeader = delayHash.Item(hdrs.id)
                        If hdrs.timestamp > old.timestamp Then
                            delayHash.Item(hdrs.id) = hdrs
                            skipList.Add(old)
                        Else
                            skipList.Add(hdrs)
                        End If
                    Else
                        'Otherwise: delay article
                        delayHash.Add(hdrs.id, hdrs)
                    End If

                ElseIf hdrs.type = "Fip-melding" And _
                 (hdrs.signature = "valuta" Or hdrs.signature.StartsWith("dnmi") Or hdrs.signature = "naaf") Then
                    'Skip misc automatic Fip messages
                    skipList.Add(hdrs)

                ElseIf hdrs.type = "Priv-melding" Or hdrs.underGruppe = "Menyer, til red." Or _
                hdrs.underGruppe = "Oversikter" Then
                    'Skip priv til red
                    skipList.Add(hdrs)

                ElseIf (hdrs.title.IndexOf("dagens-dobbel") > -1 Or hdrs.title.IndexOf("dd-") > -1 Or _
                hdrs.title.IndexOf("dagens-d-") > -1 Or hdrs.title.IndexOf("v75-") > -1 Or hdrs.title.IndexOf("v5a-") > -1 Or _
                hdrs.title.IndexOf("v5b-") > -1 Or hdrs.title.IndexOf("v5A-") > -1 Or _
                hdrs.title.IndexOf("v5B-") > -1 Or hdrs.title.IndexOf("v5-") > -1) And _
                hdrs.title.IndexOf("fotb-") = -1 And hdrs.stoffGruppe = "Sport" And _
                hdrs.underGruppe = "Tabeller og resultater" Then
                    'Skip horseracing betting results articles of all kinds
                    skipList.Add(hdrs)

                ElseIf hdrs.target = "Ut-Satellitt" And _
                (hdrs.distCode = "ALL" Or hdrs.distCode = "KUL" Or hdrs.distCode = "DID" Or hdrs.distCode = "PRS") Then
                    'Adding to hashtable for archiving if target is 'Ut-Satellitt'

                    'Is debugging enabled?
                    If enableDebug Then

                        'Retrieve id and SO for article in question
                        Dim so As String = hdrs.stikkord
                        Dim id As String = hdrs.id

                        'Compare SO with earlier SO's
                        If SOToId.ContainsKey(so) Then
                            'If found, compare ID's
                            Dim hdr As ConvertLibrary.xmlArticleHeader = SOToId(so)

                            If hdr.id <> hdrs.id Then
                                'Log event if ID is different for articles with the same SO
                                debugLog.WriteLine("Articles with equal 'Stikkord' and different ID detected:")
                                debugLog.WriteLine(hdr.fileName & vbTab & hdr.id & vbTab & hdr.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & hdr.stoffGruppe & vbTab & hdr.title)
                                debugLog.WriteLine(hdrs.fileName & vbTab & hdrs.id & vbTab & hdrs.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & hdrs.stoffGruppe & vbTab & hdrs.title)
                                debugLog.WriteLine(vbCrLf)
                            End If
                        Else
                            'Else, add to list
                            SOToId.Add(so, hdrs)
                        End If

                        'Compare ID with earlier ID's
                        If IdToSO.ContainsKey(id) Then
                            'If found, compare SO's
                            Dim hdr As ConvertLibrary.xmlArticleHeader = IdToSO(id)

                            If so <> hdr.stikkord Then
                                'Log event if SO is different for articles with the same ID
                                debugLog.WriteLine("Articles with equal ID and different 'Stikkord' detected:")
                                debugLog.WriteLine(hdr.fileName & vbTab & hdr.id & vbTab & hdr.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & hdr.stoffGruppe & vbTab & hdr.title)
                                debugLog.WriteLine(hdrs.fileName & vbTab & hdrs.id & vbTab & hdrs.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & hdrs.stoffGruppe & vbTab & hdrs.title)
                                debugLog.WriteLine(vbCrLf)
                            End If

                        Else
                            IdToSO.Add(hdrs.id, hdrs)
                        End If

                    End If 'End debug block

                    'HAST articles are always added
                    If hdrs.title.StartsWith("HAST") Then
                        'Adding HAST-article to hashtable
                        If xmlHeaderHash.ContainsKey(hdrs.longId) Then
                            errorList.Add(hdrs)
                        Else
                            xmlHeaderHash.Add(hdrs.longId, hdrs)
                        End If

                    ElseIf xmlHeaderHash.ContainsKey(hdrs.id & hdrs.stikkord & hdrs.timestamp.DayOfYear()) Then
                        'Article already exists
                        'Updating if necessary and skips old version
                        Dim old As ConvertLibrary.xmlArticleHeader = xmlHeaderHash.Item(hdrs.id & hdrs.stikkord & hdrs.timestamp.DayOfYear())
                        If hdrs.timestamp > old.timestamp Then
                            xmlHeaderHash.Item(hdrs.id & hdrs.stikkord & hdrs.timestamp.DayOfYear()) = hdrs
                            skipList.Add(old)
                        Else
                            skipList.Add(hdrs)
                        End If

                    Else
                        'Adding article to hashtable
                        xmlHeaderHash.Add(hdrs.id & hdrs.stikkord & hdrs.timestamp.DayOfYear(), hdrs)
                    End If

                Else
                    'Adding skipped file to skiplist
                    skipList.Add(hdrs)
                End If

                'Labels and goto, shame on you
Next_Item:
            Next
        End If

        'Debugging enabled? Clean everything up...
        If enableDebug Then
            debugLog.Close()
            'If Not Directory.Exists(debugLogFolder & "copy") Then Directory.CreateDirectory(debugLogFolder & "copy")
            'If File.Exists(debugLogFolder & "copy\" & debugLogFile) Then File.Delete(debugLogFolder & "copy\" & debugLogFile)
            'File.Copy(debugLogFolder & debugLogFile, debugLogFolder & "copy\" & debugLogFile)
        End If

        'Moving data to ArrayList and sorting the list
        'Dim hdr As xmlArticleHeader
        Dim itr As IDictionaryEnumerator = xmlHeaderHash.GetEnumerator()

        'Iterate through articles to archive and move them to articleList
        While itr.MoveNext()
            'Retrieve item
            'hdr = xmlHeaderHash.Item(itr.Key)
            xmlHdrList.Add(itr.Value)
        End While

        'Sort article list
        xmlHdrList.Sort()

        'Iterate through articles to delay and move them to delaylist
        itr = delayHash.GetEnumerator()
        While itr.MoveNext()
            'Retrieve item
            'hdr = xmlHeaderHash.Item(itr.Key)
            delayList.Add(itr.Value)
        End While

        'Sort delay list
        delayList.Sort()

        'Sort skiplist
        skipList.Sort()

        'Sort errorlist
        errorList.Sort()

    End Function

    'Process delayed articles
    Public Sub ProcessDelayedArticles()

        'User recursive helper to process the delayfolder
        ProcessDelayFolder(delayFolder)

    End Sub

    'Recursive helper to ProcessDelayedArticles
    Private Sub ProcessDelayFolder(ByVal folder As String)

        Dim strDate As String
        Dim file As String
        Dim files As String() = Directory.GetFiles(folder)
        Dim dir As String
        Dim dirs As String() = Directory.GetDirectories(folder)

        'Scan through subfolders to see if any delayed articles is to be archived based on folder date
        For Each dir In dirs

            'Retrieve date from foldername
            strDate = dir.Substring(dir.LastIndexOf("\") + 1)

            'If this is a date folder, only add files if the general delay has passed
            If (strDate.Split("-").Length = 3) Then
                Dim dt As Date = New Date(strDate.Substring(0, 4), strDate.Substring(5, 2), strDate.Substring(8, 2))
                dt = dt.AddDays(featureDelay)

                If (dt <= Now()) Then ProcessDelayFolder(dir)
            Else
                ProcessDelayFolder(dir)
            End If
        Next

        'Archive any delayed articles found
        For Each file In files

            'Load headers
            Dim hdr As ConvertLibrary.xmlArticleHeader
            ConvertLibrary.LoadArticleHeaders(file, hdr)

            'Transform file and write to target
            Dim output As String = ConvertLibrary.TransformArticle(hdr)
            targetFile.WriteLine(output)

            'Update datewise counter
            strDate = hdr.timestamp.ToString("yy-MM-dd")
            If dateCount.ContainsKey(strDate) Then
                dateCount(strDate) += 1
            Else
                dateCount.Add(strDate, 1)
            End If

            'Log event
            archiveLog.WriteLine("Delayed feature article: " + file.Substring(file.LastIndexOf("\") + 1))

            'Count
            archiveCount += 1

            'Move file to archive
            If moveOnArchive Then
                'Calculate folder for storage
                Dim af As String = archiveFolder & Now.ToString("yyyy-MM") & "\delayed_to_" & Now().ToString("yyyy-MM-dd") & "\"

                If Not Directory.Exists(af) Then Directory.CreateDirectory(af)
                If System.IO.File.Exists(af & file.Substring(file.LastIndexOf("\") + 1)) Then System.IO.File.Delete(af & file.Substring(file.LastIndexOf("\") + 1))
                Directory.Move(file, af & file.Substring(file.LastIndexOf("\") + 1))
            End If

        Next

    End Sub

    'Processes articles found
    Public Sub ProcessArticles()

        'Iteration
        Dim hdr As ConvertLibrary.xmlArticleHeader

        'Iterate through articles to be archived
        For Each hdr In xmlHdrList

            'Transform and write to target
            Dim output As String = ConvertLibrary.TransformArticle(hdr)
            targetFile.WriteLine(output)

            'Update datewise counter
            Dim strDate As String = hdr.timestamp.ToString("yy-MM-dd")
            If dateCount.ContainsKey(strDate) Then
                dateCount(strDate) += 1
            Else
                dateCount.Add(strDate, 1)
            End If

            'Dump to logfile
            archiveLog.WriteLine(hdr.fileName & vbTab & hdr.id & vbTab & hdr.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & hdr.stoffGruppe & vbTab & hdr.title)

            'Move archived article
            If moveOnArchive Then
                Dim af As String = archiveFolder & hdr.fileFolder.Substring(xmlFolder.Length)
                If Not Directory.Exists(af) Then Directory.CreateDirectory(af)
                If File.Exists(af & hdr.fileName) Then File.Delete(af & hdr.fileName)
                File.Move(hdr.fileFolder & hdr.fileName, af & hdr.fileName)
            End If

        Next

        'Move delayed 
        MoveDelayedArticles()

        'Move errors
        MoveErrorArticles()

        'Move Skipped
        If moveOnSkip Then MoveSkippedArticles()

        'Count
        archiveCount += xmlHdrList.Count
        archiveLog.WriteLine("Total archived: " & archiveCount)

        'Datewise count
        Dim itr As IDictionaryEnumerator = dateCount.GetEnumerator()

        'Iterate through counters
        While itr.MoveNext()
            'Print counter to file
            targetLogFile.WriteLine(itr.Key & " " & dateCount.Item(itr.Key))
        End While

        targetFile.Close()
        targetLogFile.Close()

        'Copy/rename target files
        If enableTargetCopy Then
            Dim fromFile As String = targetFolder & AppSettings("dataFileName").Replace("d%", Now().ToString("yyyy-MM-dd")) & ".txt"
            Dim fromLog As String = targetFolder & AppSettings("logFileName").Replace("d%", Now().ToString("yyyy-MM-dd")) & ".txt"

            Dim toFile As String = copyTarget & AppSettings("dataFileName").Replace("d%", Now().ToString("yyyy-MM-dd"))
            Dim toLog As String = copyTarget & AppSettings("logFileName").Replace("d%", Now().ToString("yyyy-MM-dd"))

            File.Copy(fromFile, toFile) 'Target
            File.Copy(fromLog, toLog) 'Targetlog
        End If

        'Initiate FTP transfer
        If enableFTP Then
            'Launch FTP script

            Dim st As ProcessStartInfo = New ProcessStartInfo
            Dim p As Process

            st.FileName = FTPCmd
            st.Arguments = FTPArgs & " -d " & FTPLog & " . " & copyTarget & "*.*"
            st.UseShellExecute = False
            st.CreateNoWindow = True
            st.WindowStyle = ProcessWindowStyle.Hidden

            p = System.Diagnostics.Process.Start(st)

            p.WaitForExit()

            Dim msg As MailMessage = New MailMessage

            msg.To = "arkivlogg@ntb.no"
            msg.From = "505@ntb.no"
            msg.Subject = "Atekst FTP transfer OK."
            If p.ExitCode > 0 Then
                msg.To &= ";" & errorMail
                msg.Subject = "ATekst FTP transfer FAILED!"
            End If
            p.Close()

            Dim log As StreamReader = New StreamReader(FTPLog, Encoding.GetEncoding("iso-8859-1"))
            msg.Body = log.ReadToEnd()
            log.Close()

            SmtpMail.SmtpServer = "post.ntb.no"
            SmtpMail.Send(msg)

            File.Delete(FTPLog)
        End If

        'Done!
    End Sub

    'Moving delayed files to delayfolder
    Private Sub MoveDelayedArticles()

        'Move delayed articles into appropriate folder under delayfolder
        'Variables
        Dim xmlHdr As ConvertLibrary.xmlArticleHeader
        Dim folder As String

        'Iterate through all articles in the delaylist
        For Each xmlHdr In delayList

            'Create folder for delay
            folder = delayFolder & xmlHdr.fileFolder.Substring(xmlFolder.Length)

            'Move article
            If Not Directory.Exists(folder) Then Directory.CreateDirectory(folder)
            If File.Exists(folder & xmlHdr.fileName) Then File.Delete(folder & xmlHdr.fileName)
            File.Move(xmlHdr.fileFolder & xmlHdr.fileName, folder & xmlHdr.fileName)

            'Log event
            delayLog.WriteLine(xmlHdr.fileName & vbTab & xmlHdr.id & vbTab & xmlHdr.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & xmlHdr.stoffGruppe & vbTab & xmlHdr.title)

        Next

        'Count
        delayCount += delayList.Count
        delayLog.WriteLine("Total delayed: " & delayCount)

    End Sub

    'Moving skipped articles to skipfolder
    Private Sub MoveSkippedArticles()

        'Variables
        Dim xmlHdr As ConvertLibrary.xmlArticleHeader
        Dim sf As String

        'Iterate through all articles in the skiplist
        For Each xmlHdr In skipList

            'Define folder to store skipped articles
            sf = skipFolder & xmlHdr.fileFolder.Substring(xmlFolder.Length)

            'Move skipped article
            If Not Directory.Exists(sf) Then Directory.CreateDirectory(sf)
            If File.Exists(sf & xmlHdr.fileName) Then File.Delete(sf & xmlHdr.fileName)
            File.Move(xmlHdr.fileFolder & xmlHdr.fileName, sf & xmlHdr.fileName)

            'Log event
            skipLog.WriteLine(xmlHdr.fileName & vbTab & xmlHdr.id & vbTab & xmlHdr.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & xmlHdr.stoffGruppe & vbTab & xmlHdr.title)

        Next

        'Count
        skipCount += skipList.Count
        skipLog.WriteLine("Total skipped: " & skipCount)

    End Sub

    'Moving error articles to errorfolder
    Private Sub MoveErrorArticles()

        'Variables
        Dim xmlHdr As ConvertLibrary.xmlArticleHeader
        Dim sf As String

        'Iterate through all articles in the skiplist
        For Each xmlHdr In errorList

            'Define folder to store skipped articles
            sf = errorFolder & xmlHdr.fileFolder.Substring(xmlFolder.Length)

            'Move skipped article
            If Not Directory.Exists(sf) Then Directory.CreateDirectory(sf)
            If File.Exists(sf & xmlHdr.fileName) Then File.Delete(sf & xmlHdr.fileName)
            File.Move(xmlHdr.fileFolder & xmlHdr.fileName, sf & xmlHdr.fileName)

            'Log event
            errorLog.WriteLine(xmlHdr.fileName & vbTab & xmlHdr.id & vbTab & xmlHdr.timestamp.ToString("yy-MM-dd HH:mm") & vbTab & xmlHdr.stoffGruppe & vbTab & xmlHdr.title)

        Next

        'Count
        errorCount += errorList.Count
        errorLog.WriteLine("Total erroneous: " & errorCount)

    End Sub

    'Recursive directory lister
    Private Sub ScanDir(ByVal dir As String, ByVal filter As String, ByVal recurse As Boolean)

        'Retrieving files in current directory
        Dim files As String() = Directory.GetFiles(dir, filter)

        'Moving data to the result array
        Dim f As String

        For Each f In files
            fileList.Add(f)
        Next

        'Recurse subdirs in the current directory
        If recurse Then
            Dim d As String
            Dim dirs As String() = Directory.GetDirectories(dir)

            Dim strDate As String
            Dim dt As Date

            For Each d In dirs

                'Retrieve date from foldername
                strDate = d.Substring(d.LastIndexOf("\") + 1)

                'If this is a date folder, only add files if the general delay has passed
                If (strDate.Split("-").Length = 3) Then
                    dt = New Date(strDate.Substring(0, 4), strDate.Substring(5, 2), strDate.Substring(8, 2))
                    dt = dt.AddDays(generalDelay)

                    If (dt <= Now()) Then ScanDir(d, filter, recurse)

                Else
                    ScanDir(d, filter, recurse)
                End If
            Next
        End If

    End Sub

End Class

